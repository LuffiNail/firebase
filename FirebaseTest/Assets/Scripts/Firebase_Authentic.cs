﻿using Firebase.Auth;
using Proyecto26;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Firebase_Authentic : MonoBehaviour{
    Firebase.Auth.FirebaseAuth auth;

    [Header("User_Data")]
    public TMP_InputField user_Email;
    public TMP_InputField user_Pasword;
    private string googleIdToken;
    private string googleAccessToken;
    
    private void Start() {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
       
    }
    public void LogIn() {
    
      Firebase.Auth.Credential credential =
      Firebase.Auth.GoogleAuthProvider.GetCredential(googleIdToken, googleAccessToken);
        auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
            if (task.IsCanceled) {
                Debug.LogError("SignInWithCredentialAsync was canceled.");
                return;
            }
            if (task.IsFaulted) {
                Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });


    }
}