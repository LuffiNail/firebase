﻿using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using Microsoft.Win32;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GetPlayerInfo : MonoBehaviour
{
    DatabaseReference reference;
    public TMP_InputField player;
    public Text nick;
    public Text leve;
    void Start()
    {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://fir-progra.firebaseio.com/");
        reference = FirebaseDatabase.DefaultInstance.RootReference;

        
    }

    // Update is called once per frame
    void Update()
    {
        //LoadPlayerData();
    }
    public void LoadPlayerData() {
        reference.Database
        .GetReference(player.text)
        .GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted) {
              // Handle the error...
          } else if (task.IsCompleted) {
                DataSnapshot snapshot = task.Result;
                var json = snapshot.GetRawJsonValue();
                PlayerInfo playerData = JsonUtility.FromJson<PlayerInfo>(json);
                nick.text = playerData.nick;
                leve.text = playerData.currentLevel.ToString();
                print(1);
            }
        });
        
    }
}

