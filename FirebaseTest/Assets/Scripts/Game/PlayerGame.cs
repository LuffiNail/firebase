﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;

public class PlayerGame : MonoBehaviour
{
    public static string playerName;
    public static int level;
    DatabaseReference reference;
    [Header("UI")]
    public new InputField name;


    [Header("Cave")]
    public Toggle cave1;
    public Toggle cave2;
    public Toggle cave3;
    private void Start() {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://fir-progra.firebaseio.com/");
        reference = FirebaseDatabase.DefaultInstance.RootReference;

        cave2.GetComponent<Toggle>().interactable = false;
        cave3.GetComponent<Toggle>().interactable = false;
    }
    private void Update() {
        Lockcave();
    }
    public void OnSubmit() {
        playerName = name.text;
        PostToDataBase();
    }
    public void ActiveLevel(int activeLevel) {
        level = activeLevel;
        print(level);
    }
    private void PostToDataBase() {
        PlayerInfo playerInfo = new PlayerInfo();
        string json = JsonUtility.ToJson(playerInfo);

        reference.Child("users").Child(playerInfo.nick).SetRawJsonValueAsync(json);

    }

    private void Lockcave() {
        if (cave1.isOn) {
            cave2.GetComponent<Toggle>().interactable = true;
            if(cave2.isOn)
                cave3.GetComponent<Toggle>().interactable = true;
        }
    }
}
