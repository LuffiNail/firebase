﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerInfo {

    public string nick;
    public int currentLevel;

    public PlayerInfo() {
        nick = PlayerGame.playerName;
        currentLevel = PlayerGame.level;
    }
}
